package music;

public class Main {
	public static void main(String[] args) {
		Scale s;
		int numScales = 0;
		for(int i = 0; i < Math.pow(2, 12); i++) {
			s = new Scale(i);
			String str = s.toString();
			System.out.print(str);
			if(str != "") numScales += s.possibleReps();
		}
		System.out.println(numScales);
	}
}
