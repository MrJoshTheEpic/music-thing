package music;

public class Scale {
	public boolean[] notes;
	private static final String[][] representations = {{"1", "bb2"},
													   {"#1", "b2"},
													   {"x1", "2", "bb3"},
													   {"#2", "b3", "bb4"},
													   {"x2", "3", "b4"},
													   {"#3", "4", "bb5"},
													   {"x3", "#4", "b5"},
													   {"x4", "5", "bb6"},
													   {"#5", "b6"},
													   {"x5", "6", "bb7"},
													   {"#6", "b7"},
													   {"x6", "7"}};
	private int[] currentRep;
	
	/**
	 * Creates a scale given a number between 0 and 2^12 - 1
	 * from the binary representation of the given number.
	 * The first note in the chromatic scale is the LSB and
	 * the last is the MSB.
	 * 
	 * @param n The number to be converted to a scale
	 */
	public Scale(int n) {
		if(n >= Math.pow(2,12)) {
			throw new IllegalArgumentException("n cannot be greater than 2^13 - 1");
		}
		
		notes = new boolean[12];
		for(int i = 0; i < 12; i++) {
			notes[i] = n % 2 == 1;
			n /= 2;
		}
		currentRep = new int[12];
	}
	
	@Override
	public String toString() {
		if(numNotes() < 3) return "";
		
		String s = "";
		int r = possibleReps();
		for(int i = 0; i < r; i++) {
			for(int j = 0; j < 12; j++) {
				if(notes[j]) {
					if(currentRep[j] >= representations[j].length) {
						currentRep[j] = 0;
						try {
							currentRep[nextNoteIndex(j)]++;
						} catch(ArrayIndexOutOfBoundsException a) {}
					}
					if(s == "" || s.charAt(s.length()-1) == '\n') {
						s += representations[j][currentRep[j]];
						currentRep[j]++;
					} else {
						s += "-" + representations[j][currentRep[j]];
					}
					
					
				}
			}
			s += "\n";
		}
		return s;
	}
	
	/**
	 * Finds the number of notes in this scale.
	 * 
	 * @return the number of notes in this scale.
	 */
	public int numNotes() {
		int notesActive = 0;
		for(int i = 0; i < 12; i++) {
			if(notes[i]) notesActive++;
		}
		return notesActive;
	}
	
	/**
	 * Finds the number of possible string representations of
	 * this scale based on which notes are used.
	 * 
	 * @return the number of possible representations of this scale
	 */
	public int possibleReps() {
		int reps = 1;
		for(int i = 0; i < 12; i++) {
			if(notes[i]) reps *= representations[i].length;
		}
		return reps;
	}
	
	private int nextNoteIndex(int after) {
		for(int i = after+1; i < 12; i++) {
			if(notes[i]) return i;
		}
		return -1;
	}
}
